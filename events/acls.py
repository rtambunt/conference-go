import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY




def get_picture(city, state):

    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state
    }

    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, params=params, headers=headers)

    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

def get_weather_data(city, state):

    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city,
        "appid": OPEN_WEATHER_API_KEY
    }

    response = requests.get(url, params=params)
    content = json.loads(response.content)

    lat = content[0]["lat"]
    lon = content[0]["lon"]


    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY
    }

    response = requests.get(weather_url, params=weather_params)
    content = json.loads(response.content)
    d = {
        "temperature": content["main"]["temp"],
        "weather_description": content["weather"][0]["description"]
    }

    return d
