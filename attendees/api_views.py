from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder

from .models import Attendee
from events.models import Conference

import json

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name"]

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        response = []
        attendees = Attendee.objects.all()
        for attendee in attendees:
            response.append(
                {
                    "name": attendee.name,
                    "href": attendee.get_api_url(),
                }
            )

        return JsonResponse({"attendees": response})
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse({
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": {
                "name": attendee.conference.name,
                "href": attendee.conference.get_api_url()
            }
        })
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

        Attendee.objects.filter(id=id).update(**content)

        # try:
        #     conference = Conference.objects.get(id = content["conference"])
        #     content["conference"] = conference
        # except Conference.DoesNotExist:
        #     return JsonResponse({"message": "Invalid location id"}, status=400)

        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
